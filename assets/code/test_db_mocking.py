import pytest

from db import get_penguin


def test_get_user_returns_user(monkeypatch):
    def mock():
        return [Penguin("Gentoo")]
    monkeypatch.setattr("models.Penguin.all", mock)
    assert get_penguin("should_exist") == Penguin("Gentoo")

def test_get_users_raises_on_bad_user():
    def mock():
        return []
    with pytest.raises(ValueError):
        get_penguin("not_there")
