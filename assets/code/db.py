def get_species(self, species):
    ret = models.Penguin.all().filter(species=species)

    if not ret:
        raise ValueError("Could not find species")

    return ret[0]
