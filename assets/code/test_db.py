import pytest

from db import get_user


def test_get_user_returns_user():
    assert get_user("should_exist") == User("should_exist")

def test_get_users_raises_on_bad_user():
    with pytest.raises(ValueError):
        get_user("not_there")
