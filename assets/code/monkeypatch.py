import pytest


def test_function(monkeypatch):
    def mock_function():
        return "foo"
    monkeypatch.setattr("module.function", mock_function)
