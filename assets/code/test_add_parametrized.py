import pytest

from add import add


@pytest.mark.parametrize(
    "a, b, expected",
    [(1, 2, 3), (-1, 1, 0), (-1, -3, -4), (0, 0, 0)]
)
def test_add(a, b, expected):
    assert add(a, b) == expected
