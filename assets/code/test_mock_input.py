import pytest


def test_mock_input(monkeypatch):
    def mock():
        return "rockhopper"

    monkeypatch.setattr("builtins.input", mock)

    assert input() == "rockhopper"
