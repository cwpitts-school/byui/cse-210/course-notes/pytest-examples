def add(x, y):
    """ Return the sum of x and y

    Args:
      x (int): First number
      y (int): Second number

    Returns:
      int: Sum of x and y
    """
    return x + y
