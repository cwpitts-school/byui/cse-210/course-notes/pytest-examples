TEX       := lualatex
TEXFLAGS  := -shell-escape
FILENAME  := pytest-examples
VIEWER    := zathura

.PHONY: clean
.DEFAULT_TARGET: all

all:
	${TEX} ${TEXFLAGS} ${FILENAME}
	${TEX} ${TEXFLAGS} ${FILENAME} 2>&1 >/dev/null
	${TEX} ${TEXFLAGS} ${FILENAME} 2>&1 >/dev/null

clean:
	rm -rf *.aux *.log *.toc *.idx *.pdf *.out
